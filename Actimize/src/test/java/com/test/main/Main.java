package com.test.main;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Main {

	public WebDriver driver;

	@BeforeClass
	public void beforeClass() throws IOException {
		WebDriverManager.chromedriver().version("103.0").setup();
		driver=new ChromeDriver();
		driver.manage().window().maximize();
		
	}
	@SuppressWarnings("deprecation")
	@BeforeMethod
	public void beforeMethod()  {
		driver.get("http://demo.guru99.com/test/newtours/register.php");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	@Test()
	public void f() throws InterruptedException {
	
		driver.findElement(By.xpath("//input[@name='firstName']")).sendKeys("Humayun");
		driver.findElement(By.xpath("//input[@name='lastName']")).sendKeys("Afridi");
		driver.findElement(By.xpath("//input[@name='phone']")).sendKeys("123456789");
		driver.findElement(By.xpath("//input[@name='userName']")).sendKeys("aafridi.619@gmail.com");
		driver.findElement(By.xpath("//input[@name='address1']")).sendKeys("Patna, Bihar");
		WebElement listBox=driver.findElement(By.xpath("//select[@name='country']"));
		Select select=new Select(listBox);
		select.selectByValue("INDIA");
		driver.findElement(By.xpath("//input[@name='email']")).sendKeys("afridi");
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys("123456");
		driver.findElement(By.xpath("//input[@name='confirmPassword']")).sendKeys("123456");
		WebElement element=driver.findElement(By.xpath("//input[@name='submit']"));
		
		if(element.isDisplayed()) {
			element.click();
			driver.quit();
			System.out.println("Submit button is displayed");
		}else {
			System.out.println("Submit button isn't displayed");
		}
		
		
		
		

	}
}
