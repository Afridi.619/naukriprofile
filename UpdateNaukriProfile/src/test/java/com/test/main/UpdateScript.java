package com.test.main;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.concurrent.TimeUnit;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class UpdateScript {

	public WebDriver driver;
	public WebDriverWait wait;
	Properties prop;
	FileInputStream fis;
	
	//String headline="BE Graduate|2 yr of experience DevOps process | Proficient with configuration management tools, and in developing "
		//	+ " CICD pipeline|2 yr experience as a QA tester|Experience in automated testing tools, leanft, Selenium| Familiar with Agile methodology";

	@BeforeClass
	public void beforeClass() throws IOException {
		prop=new Properties();
		fis=new FileInputStream(System.getProperty("user.dir")+"//MyFolder//Config.properties");
		prop.load(fis);
		WebDriverManager.chromedriver().version("95.0").setup();
		driver=new ChromeDriver();
		driver.manage().window().maximize();
		
	}

	@BeforeMethod
	public void beforeMethod()  {
		driver.get(prop.getProperty("url"));
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	
	public void testCase() throws InterruptedException {
		try {

			Repository r=new Repository();
			PageFactory.initElements(driver, r);
			JavascriptExecutor js=(JavascriptExecutor)driver;
			ArrayList<String>al=new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(al.get(0));
			System.out.println(driver.switchTo().window(al.get(0)).getTitle());
			wait=new WebDriverWait(driver,20);
			wait.until(ExpectedConditions.visibilityOf(r.login));
			
			if(r.login.isDisplayed()&& r.login.isEnabled()){
				Assert.assertTrue(driver.findElement(By.xpath("//div[text()='Login']")).isDisplayed());
				driver.findElement(By.xpath("//div[text()='Login']")).click();
				
				WebElement emailBox= wait.until(ExpectedConditions.visibilityOf(r.email));
				emailBox.sendKeys(prop.getProperty("email"));
				WebElement pwd= wait.until(ExpectedConditions.visibilityOf(r.pwd));
				pwd.sendKeys(prop.getProperty("pwd"));

				r.submit.click();
				Thread.sleep(5000);
				wait.until(ExpectedConditions.visibilityOf(r.naukriButton));
				
				Actions action = new Actions(driver);
				action.moveToElement(r.naukriButton).build().perform();
				
		    	System.out.println("Done Mouse hover on My Naukri");;
		    	Thread.sleep(3000);
		    
		    	r.editProfile.click();
		    	System.out.println("clicked on Edit Profile");
						
				r.updateResumeButton.sendKeys(System.getProperty("user.dir")+"//MyFolder//Afridi_CV.pdf");
		    	
		    	WebElement resume= wait.until(ExpectedConditions.visibilityOf(r.resumeHeadline));
				js.executeScript("arguments[0].click();", resume);

				WebElement resumeText= wait.until(ExpectedConditions.visibilityOf(r.resumeHeadlineTextBox));
				String text=resumeText.getAttribute("value");

				System.out.println("Resume Headline: "+text);

				if (text.contentEquals(prop.getProperty("headline"))) {
					resumeText.clear();
					resumeText.sendKeys(prop.getProperty("headline")+".");
					r.saveButton.click();
					Thread.sleep(3000);
					System.out.println("Updated Resume headline: "+text);
					driver.quit();
				}else if(text.endsWith(".")) {
					resumeText.clear();
					resumeText.sendKeys(prop.getProperty("headline"));
					r.saveButton.click();
					Thread.sleep(3000);
					System.out.println("Updated Resume headline: "+text);
					driver.quit();
					
				} else {
					text.contains(prop.getProperty("headline"));
					resumeText.clear();
					resumeText.sendKeys(prop.getProperty("headline")+".");
					r.saveButton.click();
					Thread.sleep(3000);
					System.out.println("Updated Resume headline: "+text);
					driver.quit();
				}
			}else {
				System.out.println("Not updated");
				driver.quit();
			}
		}catch(Exception e) {
			driver.quit();
			e.printStackTrace();
		}
	}


}
