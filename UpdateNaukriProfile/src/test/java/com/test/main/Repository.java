package com.test.main;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Repository {
	@FindBy(xpath="//div[text()='Login']")
	public WebElement login;
	
	@FindBy(xpath="//*[@id='root']//div/form/div[2]/input")
	public WebElement email;
	
	@FindBy(xpath="//*[@id='root']//div/form/div[3]/input")
	public WebElement pwd;
	
	@FindBy(xpath="//button[@type='submit']")
	public WebElement submit;
	
	//@FindBy(xpath="//*[@class='col s4 sidebar']//div/img")
	
	@FindBy(xpath="//div[@class='mTxt' and text()='My Naukri']")
	public WebElement naukriButton;

	
	@FindBy(xpath="//div[@class='commonHeaderFooter']//li/a[ contains(text(),'Edit Profile')]")
	public WebElement editProfile;

	
	@FindBy(xpath="//*[@class='resumeHeadline']//span[2]")
	public WebElement resumeHeadline;
	
	@FindBy(xpath="//textarea[@id='resumeHeadlineTxt']")
	public WebElement resumeHeadlineTextBox;
	
	
	@FindBy(xpath="//*[@class='action s12']//button")
	public WebElement saveButton;
	
	@FindBy(xpath="//div[@class='cvPreview']//span")
	public WebElement updateMsg;
	
	@FindBy(xpath="//input[@id='attachCV']")
	public WebElement updateResumeButton;
	
	

}
